require 'capistrano/ext/multistage'
require 'bundler/capistrano'

set :stages, %w('production', 'homolog')
set :default_stage, "homolog"

set :application, 'PUG'

set :scm, :git
set :repository, 'git@bitbucket.org:seseg/pug.git'
set :branch, "master"
set :use_sudo, false
set :user, 'applications'

set :deploy_to, "/home/#{user}/apps/#{application}"

set :unicorn_user, user
set :unicorn_pid, "#{current_path}/tmp/pids/unicorn.pid"
set :unicorn_config, "#{shared_path}/config/unicorn.rb"
set :unicorn_log, "#{shared_path}/log/unicorn.log"
set :unicorn_workers, 2


default_run_options[:pty] = true
ssh_options[:forward_agent] = true

namespace :deploy do
  %w[start stop restart].each do |command|                                # overrides start, stop and restart tasks
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "/etc/init.d/unicorn_#{application} #{command}"                 # cap tasks are proxied to unicorn service
    end
  end

  task :setup_config, roles: :app do
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
    run "mkdir -p #{shared_path}/config"
  end
  after "deploy:setup", "deploy:setup_config"

  desc "Creating mongoid.yml file into latest release"
  task :config_database, roles: :app do
    run "cp #{release_path}/config/mongoid.example.yml #{release_path}/config/mongoid.yml"
  end
  before "deploy:finalize_update", "deploy:config_database"

end
