namespace :foreman do
  desc "Export the Procfile to Ubuntu's upstart scripts"
  task :export, roles: :app do
    run "cd #{current_path}; bundle exec foreman export upstart #{current_path}/config/ -a #{application} -u #{user} -l #{release_path}/log/foreman"
    sudo "mv #{current_path}/config/#{application}*.conf /etc/init/"
  end

  desc "Start the application services"
  task :start, roles: :app do
    run "start #{application}"
  end

  desc "Stop the application services"
  task :stop, roles: :app do
    run "stop #{application}"
  end

  desc "Restart the application services"
  task :restart, roles: :app do
    sudo "start #{application}"
  end
end

after "deploy:update", "foreman:export"
after "deploy:update", "foreman:restart"