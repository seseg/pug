require 'mongoid'

class Log
  include Mongoid::Document

  field :ip
  field :user_id
  field :http_verb
  field :url
  field :status_code
  field :error
  field :action
  field :created_at, type: DateTime

  scope :ip,->(ip){ where(ip: ip) }
  scope :user_id,->(user_id){ where(user_id: user_id) }
  scope :status,->(status_code){ where(status_code: status_code) }
  scope :http_verb,->(http_verb){ where(http_verb: http_verb) }
  scope :action,->(action){ where(action: action) }
  scope :created_at_range,->(from, to){ where(:created_at.gte => from, :created_at.lte => to+1.day-1.second) }

  belongs_to :node

  def self.filter(params)
    @logs = Log
    @logs = @logs.where(node_id: params['node'])
    @logs = @logs.ip(params['ip']) if params['ip'] && !params['ip'].empty?
    @logs = @logs.user_id(params['user_id']) if params['user_id'] && !params['user_id'].empty?
    @logs = @logs.status(params['status_code']) if params['status_code'] && !params['status_code'].empty?
    @logs = @logs.http_verb(params['http_verb']) if params['http_verb'] && !params['http_verb'].empty?
    @logs = @logs.action(params['action']) if params['action'] && !params['action'].empty?

    if params['from'] && !params['from'].empty?
      if params['to'] && !params['to'].empty?
        @logs = @logs.created_at_range(Date.parse(params['from']), Date.parse(params['to']))
      else
        @logs = @logs.created_at_range(Date.parse(params['from']), Date.parse(params['from']))
      end
    end

    @logs = @logs.desc(:created_at)
    @logs
  end

end