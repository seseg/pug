require 'mongoid'

class Node
  include Mongoid::Document

  field :name
  field :environment
  field :ip
  field :url

  has_many :logs
end