$.fn.serializeObject = ->
  arrayData = @serializeArray()
  objectData = {}

  $.each arrayData, ->
    value = @value || ''
    if objectData[@name]?
      unless objectData[@name].push
        objectData[@name] = [objectData[@name]]
      objectData[@name].push value
    else
      objectData[@name] = value

  return objectData

$ ->
  $("#nav-left, #nav-right").mousedown ->
    event.preventDefault()

  $("#nav-right").click (e) ->
    currentPage = parseInt($("form").find("input[name=page]").val())
    pageCount = parseInt($("form").find("input[name=page_count]").val())
    nextPage = currentPage + 1
    nextPage = currentPage if nextPage > pageCount
    $("form").find("input[name=page]").val(nextPage)
    refresh() unless currentPage == nextPage

  $("#nav-left").click (e) ->
    currentPage = parseInt($("form").find("input[name=page]").val())
    previousPage = currentPage - 1
    previousPage = 0 if previousPage < 0
    $("form").find("input[name=page]").val(previousPage)
    refresh() unless currentPage == previousPage

  refresh = ->
    formData = $("form").serializeObject()
    $.getJSON "/#{formData['node']}/logs", formData, (data) ->
      template = _.template $("#log").html()
      [count, json] = [data...]
      $("form").find("input[name=page_count]").val count
      $("table tbody").html(_.map json, (item) -> template(item))

  refresh()
  $("form :input").change refresh
  setInterval refresh, 5000
