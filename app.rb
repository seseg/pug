require 'rubygems'
require 'sinatra'
require "sinatra/reloader" if development?
require 'sinatra/respond_with'
require 'sinatra/json'
require 'mongoid'
require './app/models/log'
require './app/models/node'

set :views, Proc.new { File.join(root, "app/views") }
set :json_encoder, :to_json

use Rack::Auth::Basic do |username, password|
  username == 'admin' && password == 'password'
end

before do
  Mongoid.load!("./config/mongoid.yml")
end

get '/application.js' do
  coffee :application
end

get '/application.css' do
  sass :application
end

get '/' do
  @nodes = Node.all
  haml :index
end

get '/:node/logs' do
  @logs = Log.filter(params)
  [@logs.count/12, @logs.offset(params['page'].to_i*12 || 0).limit(12)].to_json
end

get '/load_dummy' do
  Node.delete_all
  Log.delete_all

  nodes = 3.times.map do |n|
    Node.create! name: "Application ##{n}"
  end

  ips = ["127.0.0.1", "10.233.20.25", "10.233.20.73"]
  dates = [Date.today+1.hour, Date.today+1.day+1.hour, Date.today-1.day+1.hour]
  user_ids = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
  responses = [["500", "ServerError"], ["404", "Not Found"], ["200", ""]]
  requests = [["GET", "/candidates"], ["POST", "/employees"], ["PUT", "/candidate/1"]]

  1000.times do |n|
    response = responses.sample
    request = requests.sample

    Log.create created_at: dates.sample,node: nodes.sample, ip: ips.sample, user_id: user_ids.sample, status_code: response.first, error: response.last, http_verb: request.first, action: request.last
  end

  ""
end