require 'stomp'
require 'json'
require 'yaml'
require 'mongoid'
require './app/models/log'
require './app/models/node'

module Stomp
  class << self
    def get_logs_from_queue
      ENV['RACK_ENV'] ||= 'development'

      config = YAML::load(File.open("./config/stomp.yml"))['development']

      Mongoid.load!("./config/mongoid.yml")

      queue = config['queue']
      server = config['server']
      user = config['user']
      password = config['password']

      client = Stomp::Client.new user, password, server, 61613
      client.subscribe queue, { :ack => :client } do | message |
        log = JSON.parse(message.body)
        log['ip'] = log.delete('remote_addr')
        log['created_at'] = log.delete('time')
        log['error'] = log.delete('exception')

        node = Node.find_or_create_by(name: log.delete('application_identifier'))
        Log.create log.merge(node: node)
        p log
        client.acknowledge message
      end
      client.join
      client.close
    end
  end
end

